package delivery;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author 200217AM
 *出前シュミレーション
 */
public class Menu{
	
	public static void main(String[] args) {
		
		//スタート画面
		System.out.println("どれにしますか？");
		
		//メニューを表示
		System.out.println("選びたい項目の番号を入力してください");
		System.out.println("\n");
		System.out.println("※全品統一価格となっております");
		System.out.println("牛丼1000円" + "  " + "ラーメン1200円" + "  " + "タピオカ600円" + "  " + "弁当800円");
		String[] manu = {"1:牛丼" + "2:ラーメン" + "3:タピオカ" + "4:弁当"};
		System.out.println(Arrays.toString(manu));
		
		//メニュー選択
		int num = new Scanner(System.in).nextInt();
		switch(num){
		
		//牛丼
		case 1:
			beefBowl();
		break;	
		
		//ラーメン
		case  2: 
			ramen();
		break;
			
		
		//タピオカ
		case 3:
			tapioca();
		break;
			
		
		//弁当
		case 4:
			lunchBox();
		break;
		}
		
	}

	
	//牛丼メソッド
	public static void beefBowl() {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("牛丼ですね");
		System.out.println("種類をお選びください");
		
		//1秒止めるメソッド
		try{
			Thread.sleep(1000); //1秒止める
		}catch(InterruptedException e){}
		
		//選択画面
	String[] beefBowl = {"1:牛丼 2:キムチ牛丼 3:おろしポン酢牛丼"};
	System.out.println(Arrays.toString(beefBowl));
	System.out.println();
	int num1 = new Scanner(System.in).nextInt();
	System.out.println(num1 + "ですね");
	
		//個数入力画面
	System.out.println("購入個数を入力してください");
	int num11 = new Scanner(System.in).nextInt();
	System.out.println(num11 + "個ですね");

		//お会計
	System.out.println("お会計します");
	System.out.println("もう一度購入個数を入力してください");
	int num111 = new Scanner(System.in).nextInt();
	System.out.println("お会計" + 1000 * num111 + "円です");
	
	
	}
	
	
	//ラーメンメソッド
	public static void ramen() {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("ラーメンですね");
		System.out.println("種類をお選びください");
		
		//1秒止めるメソッド1

		try{
			Thread.sleep(1000); //1秒止める
		}catch(InterruptedException e){}
		
		//選択画面
		String[] ramen = {"1:豚骨ラーメン 2:醬油ラーメン 3:味噌ラーメン 4:塩ラーメン"};
		System.out.println(Arrays.toString(ramen));
		int num2 = new Scanner(System.in).nextInt();
		System.out.println(num2 + "ですね");
		
		
		//個数入力画面
		System.out.println("購入個数を入力してください");
		int num22 = new Scanner(System.in).nextInt();
		System.out.println(num22 + "個ですね");
	
		//お会計
		System.out.println("お会計します");
		System.out.println("もう一度購入個数を入力してください");
		int num222 = new Scanner(System.in).nextInt();
		System.out.println("お会計" + 1200 * num222 + "円です");
	}
	
	
	//タピオカメソッド
	public static void tapioca() {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("タピオカですね");
		System.out.println("種類をお選びください");
		
		//1秒止めるメソッド
		try{
			Thread.sleep(1000); //1秒止める
		}catch(InterruptedException e){}
			
		//選択画面
		String[] tapioca = {"1:ミルクティー 2:抹茶 3:アップル"};
		System.out.println(Arrays.toString(tapioca));
		int num3 = new Scanner(System.in).nextInt();
		System.out.println(num3 + "ですね");
		
		
		//個数入力画面
		System.out.println("購入個数を入力してください");
		int num33 = new Scanner(System.in).nextInt();
		System.out.println(num33 + "個ですね");
	
		//お会計
		System.out.println("お会計します");
		System.out.println("もう一度購入個数を入力してください");
		int num333 = new Scanner(System.in).nextInt();
		System.out.println("お会計" + 600 * num333 + "円です");
		
	}

	//お弁当メソッド
	public static void lunchBox() {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("弁当ですね");
		System.out.println("種類をお選びください");
		
		//1秒止めるメソッド
		try{
			Thread.sleep(1000); //1秒止める
		}catch(InterruptedException e){}
			
	//選択画面
		String[] lunchBox = {"1:からあげ弁当 2:日の丸弁当 3:のり弁"};
		System.out.println(Arrays.toString(lunchBox));
		int num4 = new Scanner(System.in).nextInt();
		System.out.println(num4 + "ですね");
		
		
		//個数入力画面
		System.out.println("購入個数を入力してください");
		int num44 = new Scanner(System.in).nextInt();
		System.out.println(num44 + "個ですね");
	
	//お会計
		System.out.println("お会計します");
		System.out.println("もう一度購入個数を入力してください");
		int num444 = new Scanner(System.in).nextInt();
		System.out.println("お会計" + 800 * num444 + "円です");
	}
		
	
}
