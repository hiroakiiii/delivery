package delivery;

import java.util.Arrays;
import java.util.Scanner;

public class Tapioca {
	public static void tapioca() {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("タピオカですね");
		System.out.println("種類をお選びください");
		
		//1秒止めるメソッド
		try{
			Thread.sleep(1000); //1秒止める
		}catch(InterruptedException e){}
			
		//選択画面
		String[] tapioca = {"1:ミルクティー 2:抹茶 3:アップル"};
		System.out.println(Arrays.toString(tapioca));
		int num3 = new Scanner(System.in).nextInt();
		System.out.println(num3 + "ですね");
		
		
		//個数入力画面
		System.out.println("購入個数を入力してください");
		int num33 = new Scanner(System.in).nextInt();
		System.out.println(num33 + "個ですね");
	
		//お会計
		System.out.println("お会計します");
		System.out.println("もう一度購入個数を入力してください");
		int num333 = new Scanner(System.in).nextInt();
		System.out.println("お会計" + 600 * num333 + "円です");
		
	}
}
