package delivery;

import java.util.Arrays;
import java.util.Scanner;

public class Ramen {
	public static void ramen() {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("ラーメンですね");
		System.out.println("種類をお選びください");
		
		//1秒止めるメソッド
		try{
			Thread.sleep(1000); //1秒止める
		}catch(InterruptedException e){}
		
		//選択画面
		String[] ramen = {"1:豚骨ラーメン 2:醬油ラーメン 3:味噌ラーメン 4:塩ラーメン"};
		System.out.println(Arrays.toString(ramen));
		int num2 = new Scanner(System.in).nextInt();
		System.out.println(num2 + "ですね");
		
		
		//個数入力画面
		System.out.println("購入個数を入力してください");
		int num22 = new Scanner(System.in).nextInt();
		System.out.println(num22 + "個ですね");
	
		//お会計
		System.out.println("お会計します");
		System.out.println("もう一度購入個数を入力してください");
		int num222 = new Scanner(System.in).nextInt();
		System.out.println("お会計" + 1200 * num222 + "円です");
	}
}
